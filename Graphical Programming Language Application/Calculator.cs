﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Graphical_Programming_Language_Application
{
    public partial class Calculator : Form
    {
        public Calculator()
        {
            InitializeComponent();
        }

        Double resultValue = 0;
        String operationPerformed = "";
        bool isOperationPerformed = false;

        private void Calculator_Load(object sender, EventArgs e)
        {

        }
        private void button_ClearEntry(object sender, EventArgs e)
        {
            textResult.Text = "0";
        }

        private void button_Clear(object sender, EventArgs e)
        {
            textResult.Text = "0";
            resultValue = 0;
        }

        private void button_EqualTo(object sender, EventArgs e)
        {
            switch (operationPerformed)
            {
                case "+":
                    textResult.Text = (resultValue + Double.Parse(textResult.Text)).ToString();
                    break;
                case "-":
                    textResult.Text = (resultValue - Double.Parse(textResult.Text)).ToString();
                    break;
                case "*":
                    textResult.Text = (resultValue * Double.Parse(textResult.Text)).ToString();
                    break;
                case "/":
                    textResult.Text = (resultValue / Double.Parse(textResult.Text)).ToString();
                    break;
                default:
                    break;
            }
            resultValue = Double.Parse(textResult.Text);
            labelCurrentOperation.Text = "";
        }

        private void button_Operation(object sender, EventArgs e)
        {
            Button button = (Button)sender;

            if (resultValue != 0)
            {
                button20.PerformClick();//button_20 is equal_button
                operationPerformed = button.Text;
                labelCurrentOperation.Text = resultValue + " " + operationPerformed;
                //for operation clicked
                isOperationPerformed = true;
            }
            else
            {
                operationPerformed = button.Text;
                resultValue = Double.Parse(textResult.Text);

                labelCurrentOperation.Text = resultValue + " " + operationPerformed;

                //for operation clicked
                isOperationPerformed = true;
            }
        }



        private void button_Number(object sender, EventArgs e)
        {
            if ((textResult.Text == "0") || (isOperationPerformed))
                textResult.Clear();

            //for number clicked
            isOperationPerformed = false;
            Button button = (Button)sender;

            if (button.Text == ".")
            {
                if (!textResult.Text.Contains("."))
                    textResult.Text = textResult.Text + button.Text;
            }
            else
                textResult.Text = textResult.Text + button.Text;

        }




    }
}
