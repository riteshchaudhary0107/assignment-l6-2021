﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Windows.Forms;

namespace Graphical_Programming_Language_Application
{
    public class Canvas
    {
        //Graphics reference of the bitmap's graphics to draw onto
        public Graphics graphics;

        //Pen reference to pen object that draws on the bitmap's graphics
        public Pen pen;

        //x-coordinate of the bitmap
        private int xPos;

        //y-coordinate of the bitmap
        private int yPos;

        //sets and returns the value of x-coordinate
        public int XPos { get => xPos; set => xPos = value; }

        //sets and returns the value of y-coordinate
        public int YPos { get => yPos; set => yPos = value; }

        //set for fill color
        public bool fillShape;

        //default constructor
        public Canvas()
        {
            Bitmap b = new Bitmap(800, 400);
            this.graphics = Graphics.FromImage(b);

            fillShape = false;
            // this.graphics = graphics;
            this.xPos = this.yPos = 0;
            pen = new Pen(Color.Black, 2);

            //fillShape = false;
        }

        //Default Parameterized constructor taking graphics object of the bitmap to draw onto
        public Canvas(Graphics graphics)
        {
            this.graphics = graphics; //assigns the reference to bitmap's graphics object
            this.xPos = this.yPos = 0; //initial cursor position set to 0,0 
            pen = new Pen(Color.Black, 2); //pen object to draw on the bitmap graphics

            fillShape = false;
        }

        //sets the cursor pointer to the coordinates provided
        public void moveTo(int x, int y)
        {
            //sets the coordinates to the provided values
            this.xPos = x;
            this.yPos = y;
        }

        //draws a line from current position to provided arguments (toX, toY)
        public void drawTo(int x, int y)
        {
            graphics.DrawLine(pen, XPos, YPos, x, y); //draws line from current to provided coordinates
            //sets the cursur coordinates to the line at the end of the line
            this.xPos = x;
            this.yPos = y;
        }

        //draws a square around the current position (xPos, yPos)
        public void drawSquare(int width)
        {
            //if fill is on
            if (this.fillShape)
            {
                graphics.FillRectangle(new SolidBrush(pen.Color), (this.xPos - width / 2), (this.yPos - width / 2), width, width);
            }
            //draws a hollow square around the cursor pointer with the width provided
            else
                graphics.DrawRectangle(pen, (this.xPos - width / 2), (this.yPos - width / 2), width, width);
        }

        //draws a rectangle around the current position (xPos, yPos)
        public void drawRectangle(int width, int height)
        {
            //if fill is on
            if (fillShape)
            {
                graphics.FillRectangle(new SolidBrush(pen.Color), (this.xPos - width / 2), (this.yPos - height / 2), width, height);
            }
            //draws a rectangle around the cursor pointer with the width and height provided
            else
                graphics.DrawRectangle(pen, (this.xPos - width / 2), (this.yPos - height / 2), width, height);
        }

        //draws a circle around the current position (xPos, yPos)
        public void drawCircle(int radius)
        {
            //if fill is on
            if (this.fillShape)
            {
                graphics.FillEllipse(new SolidBrush(pen.Color), (this.xPos - radius), (this.yPos - radius), (radius * 2), (radius * 2));
            }
            //draws a circle around the cursor pointer with the radius provided
            else
                graphics.DrawEllipse(pen, (this.xPos - radius), (this.yPos - radius), (radius * 2), (radius * 2));
        }

        //draws a triangle from the current position (xPos, yPos)
        public void drawTriangle(int width, int height)
        {
            //draws a triangle around the cursor pointer with the width and height provided
            int x = this.xPos - width / 2;
            int y = this.yPos + height / 2;
            graphics.DrawLine(pen, x, y, x + width, y);
            graphics.DrawLine(pen, x + width, y, this.xPos, this.yPos - height / 2);
            graphics.DrawLine(pen, this.xPos, this.yPos - height / 2, x, y);
        }

        //draws a triangle from the current position (xPos, yPos)
        public void drawHalfTriangle(int width, int height)
        {
            //draws a triangle around the cursor pointer with the width and height provided
            int x = this.xPos - width / 2;
            int y = this.yPos + height / 2;
            graphics.DrawLine(pen, x, y, x + width, y);
            graphics.DrawLine(pen, x + width, y, this.xPos, this.yPos);
            graphics.DrawLine(pen, this.xPos, this.yPos, x, y);
        }

        //runs the commands on the multi line command box
        public void run(RichTextBox multiCommandLine)
        {
            if (multiCommandLine.Lines.Length > 1)
            {
                multiCommandLine.Text = "Multiple Lines => Line 1: " + multiCommandLine.Lines[0] + "\n Line 2: " + multiCommandLine.Lines[1];
            }
            else
            {
                multiCommandLine.Text = "Single Lines";
            }
        }

        //sets pen size to passed size
        public void setPenSize(int pSize, int lineNo)
        {
            //checks if the pen size is within a valid range and throws an exception if not
            if (pSize < 0 || pSize > 20)
                throw new InvalidPenSizeException(lineNo);
            this.pen.Width = pSize;
        }


        //sets pen color to passed color
        public void setPenColor(String color, int lineNo)
        {
            color = color.ToLower();

            if (color.Equals("red"))
            {
                pen.Color = Color.Red;
            }
            else if (color.Equals("yellow"))
            {
                pen.Color = Color.Yellow;
            }
            else if (color.Equals("green"))
            {
                pen.Color = Color.Green;
            }
            else if (color.Equals("blue"))
            {
                pen.Color = Color.Blue;
            }
            else
            {
                throw new InvalidColorException(lineNo);
            }
        }

        //Turns the fill on and off for the shapes
        public void setFillStatus(String fStatus, int lineNo)
        {
            if (fStatus.ToLower() == "on")
                this.fillShape = true;
            else if (fStatus.ToLower() == "off")
                this.fillShape = false;
            else
                throw new ParameterFormatException(lineNo);
        }

        //clears the bitmap's graphics of the picturebox
        public void clear()
        {
            //clears the output bitmap's graphics and refills it with white color
            graphics.Clear(Color.White);
        }

        //sets default pen  and solid brush
        public void reset()
        {  //initial cursor position set to 0,0 
            clear();
            this.xPos = 0;
            this.yPos = 0;
            pen = new Pen(Color.Black, 1);//default fill color for solid brush

        }
    }
}
