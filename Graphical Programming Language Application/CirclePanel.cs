﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace Graphical_Programming_Language_Application
{
    class CirclePanel : ShapesPanel
    {
        private int x, y;
        private const int size = 20;

        public void SetPosition(int x, int y)
        {
            this.x = x;
            this.y = y;
        }

        //Need a Pen onject and a Graphics object
        public void Draw(Graphics g, Pen myPen)
        {
            //no need to 'override' method in an interface
            g.DrawEllipse(myPen, x - size, y - size, size * 2, size * 2);
            //Other parameters are x and y position,
            //and the horizontal and verticle size of the circle

        }

        public override string ToString()
        {
            return "Circle class, x, y, size = " + x + " " + y;
        }
    }
}
