﻿
namespace Graphical_Programming_Language_Application
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.assignmentToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.practiceToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.aSEToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.assignmentToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.practiceToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.studentRegFormToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.calculatorToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.panelClickToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.optionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.hintToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.contextMenuStrip1.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.assignmentToolStripMenuItem,
            this.practiceToolStripMenuItem});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(156, 52);
            // 
            // assignmentToolStripMenuItem
            // 
            this.assignmentToolStripMenuItem.Name = "assignmentToolStripMenuItem";
            this.assignmentToolStripMenuItem.Size = new System.Drawing.Size(155, 24);
            this.assignmentToolStripMenuItem.Text = "Assignment";
            // 
            // practiceToolStripMenuItem
            // 
            this.practiceToolStripMenuItem.Name = "practiceToolStripMenuItem";
            this.practiceToolStripMenuItem.Size = new System.Drawing.Size(155, 24);
            this.practiceToolStripMenuItem.Text = "Practice";
            // 
            // menuStrip1
            // 
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.aSEToolStripMenuItem,
            this.practiceToolStripMenuItem1,
            this.optionToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1262, 28);
            this.menuStrip1.TabIndex = 2;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // aSEToolStripMenuItem
            // 
            this.aSEToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.assignmentToolStripMenuItem1});
            this.aSEToolStripMenuItem.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.aSEToolStripMenuItem.Name = "aSEToolStripMenuItem";
            this.aSEToolStripMenuItem.Size = new System.Drawing.Size(49, 24);
            this.aSEToolStripMenuItem.Text = "ASE";
            this.aSEToolStripMenuItem.Click += new System.EventHandler(this.aSEToolStripMenuItem_Click);
            // 
            // assignmentToolStripMenuItem1
            // 
            this.assignmentToolStripMenuItem1.Name = "assignmentToolStripMenuItem1";
            this.assignmentToolStripMenuItem1.Size = new System.Drawing.Size(117, 26);
            this.assignmentToolStripMenuItem1.Text = "GPL";
            this.assignmentToolStripMenuItem1.Click += new System.EventHandler(this.assignmentToolStripMenuItem1_Click);
            // 
            // practiceToolStripMenuItem1
            // 
            this.practiceToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.studentRegFormToolStripMenuItem,
            this.calculatorToolStripMenuItem,
            this.panelClickToolStripMenuItem});
            this.practiceToolStripMenuItem1.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.practiceToolStripMenuItem1.Name = "practiceToolStripMenuItem1";
            this.practiceToolStripMenuItem1.Size = new System.Drawing.Size(75, 24);
            this.practiceToolStripMenuItem1.Text = "Practice";
            // 
            // studentRegFormToolStripMenuItem
            // 
            this.studentRegFormToolStripMenuItem.Name = "studentRegFormToolStripMenuItem";
            this.studentRegFormToolStripMenuItem.Size = new System.Drawing.Size(214, 26);
            this.studentRegFormToolStripMenuItem.Text = "Student Reg. Form";
            this.studentRegFormToolStripMenuItem.Click += new System.EventHandler(this.studentRegFormToolStripMenuItem_Click);
            // 
            // calculatorToolStripMenuItem
            // 
            this.calculatorToolStripMenuItem.Name = "calculatorToolStripMenuItem";
            this.calculatorToolStripMenuItem.Size = new System.Drawing.Size(214, 26);
            this.calculatorToolStripMenuItem.Text = "Calculator";
            this.calculatorToolStripMenuItem.Click += new System.EventHandler(this.calculatorToolStripMenuItem_Click);
            // 
            // panelClickToolStripMenuItem
            // 
            this.panelClickToolStripMenuItem.Name = "panelClickToolStripMenuItem";
            this.panelClickToolStripMenuItem.Size = new System.Drawing.Size(214, 26);
            this.panelClickToolStripMenuItem.Text = "Panel Click";
            this.panelClickToolStripMenuItem.Click += new System.EventHandler(this.panelClickToolStripMenuItem_Click);
            // 
            // optionToolStripMenuItem
            // 
            this.optionToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.hintToolStripMenuItem,
            this.exitToolStripMenuItem});
            this.optionToolStripMenuItem.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.optionToolStripMenuItem.Name = "optionToolStripMenuItem";
            this.optionToolStripMenuItem.Size = new System.Drawing.Size(69, 24);
            this.optionToolStripMenuItem.Text = "Option";
            // 
            // hintToolStripMenuItem
            // 
            this.hintToolStripMenuItem.Name = "hintToolStripMenuItem";
            this.hintToolStripMenuItem.Size = new System.Drawing.Size(120, 26);
            this.hintToolStripMenuItem.Text = "Hint";
            this.hintToolStripMenuItem.Click += new System.EventHandler(this.hintToolStripMenuItem_Click);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(120, 26);
            this.exitToolStripMenuItem.Text = "Exit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Window;
            this.ClientSize = new System.Drawing.Size(1262, 703);
            this.Controls.Add(this.menuStrip1);
            this.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.IsMdiContainer = true;
            this.MainMenuStrip = this.menuStrip1;
            this.MaximizeBox = false;
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.contextMenuStrip1.ResumeLayout(false);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem assignmentToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem practiceToolStripMenuItem;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem aSEToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem assignmentToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem practiceToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem studentRegFormToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem calculatorToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem optionToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem panelClickToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem hintToolStripMenuItem;
    }
}

