﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Graphical_Programming_Language_Application
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void aSEToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void panelClickToolStripMenuItem_Click(object sender, EventArgs e)
        {
            PanelClick panelClick = new PanelClick();
            panelClick.MdiParent = this;
            panelClick.Show();
        }

        private void studentRegFormToolStripMenuItem_Click(object sender, EventArgs e)
        {
            StudentRegis student = new StudentRegis();
            student.MdiParent = this;
            student.Show();
        }

        private void calculatorToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Calculator cal = new Calculator();
            cal.MdiParent = this;
            cal.Show();
        }

        private void assignmentToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            GPL gpl = new GPL();
            gpl.MdiParent = this;
            gpl.Show();
        }

        private void hintToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("drawTo -> Position indicating by line "
                           + "\nmoveTo -> Position indicating by point : "
                           + "\nfill -> on or off : "
                           + "\npencolor -> red | yellow | green | blue : "
                           + "\ncircle value1 -> draw Circle : "
                           + "\nsquare value1 -> draw Square : "
                           + "\nrect value1, value2 -> draw rectangle : "
                           + "\ntriangle value1, value2 -> draw Triangle : "
                           + "\nhalftriangle value1, value2 -> draw Triangle : ");
        }
    }
}
