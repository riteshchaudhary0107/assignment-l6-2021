﻿
namespace Graphical_Programming_Language_Application
{
    partial class GPL
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(GPL));
            this.fileNameLabel = new System.Windows.Forms.Label();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.loadMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.multiCommandLine = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.fileNamePanel = new System.Windows.Forms.Panel();
            this.fileCloseBtn = new System.Windows.Forms.Button();
            this.errorDisplay = new System.Windows.Forms.RichTextBox();
            this.errorHeaderLabel = new System.Windows.Forms.Label();
            this.syntaxBtn = new System.Windows.Forms.Button();
            this.saveDrawMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.loadDrawToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.fileNamePanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // fileNameLabel
            // 
            this.fileNameLabel.AutoSize = true;
            this.fileNameLabel.BackColor = System.Drawing.Color.Black;
            this.fileNameLabel.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.fileNameLabel.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.fileNameLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.fileNameLabel.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.fileNameLabel.Location = new System.Drawing.Point(5, 4);
            this.fileNameLabel.Name = "fileNameLabel";
            this.fileNameLabel.Size = new System.Drawing.Size(116, 22);
            this.fileNameLabel.TabIndex = 24;
            this.fileNameLabel.Text = "Untitled.gpl.txt";
            // 
            // menuStrip1
            // 
            this.menuStrip1.GripStyle = System.Windows.Forms.ToolStripGripStyle.Visible;
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileMenu});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.RenderMode = System.Windows.Forms.ToolStripRenderMode.System;
            this.menuStrip1.Size = new System.Drawing.Size(1162, 30);
            this.menuStrip1.TabIndex = 23;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileMenu
            // 
            this.fileMenu.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.loadMenuItem,
            this.saveMenuItem,
            this.saveDrawMenuItem,
            this.loadDrawToolStripMenuItem});
            this.fileMenu.ForeColor = System.Drawing.SystemColors.ControlText;
            this.fileMenu.Name = "fileMenu";
            this.fileMenu.Size = new System.Drawing.Size(46, 26);
            this.fileMenu.Text = "File";
            this.fileMenu.MouseEnter += new System.EventHandler(this.fileMenu_MouseEnter);
            this.fileMenu.MouseLeave += new System.EventHandler(this.fileMenu_MouseLeave);
            // 
            // loadMenuItem
            // 
            this.loadMenuItem.Name = "loadMenuItem";
            this.loadMenuItem.Size = new System.Drawing.Size(224, 26);
            this.loadMenuItem.Text = "Load";
            this.loadMenuItem.Click += new System.EventHandler(this.loadMenuItem_Click);
            // 
            // saveMenuItem
            // 
            this.saveMenuItem.Name = "saveMenuItem";
            this.saveMenuItem.Size = new System.Drawing.Size(224, 26);
            this.saveMenuItem.Text = "Save File";
            this.saveMenuItem.Click += new System.EventHandler(this.saveFileMenuItem_Click);
            // 
            // multiCommandLine
            // 
            this.multiCommandLine.Location = new System.Drawing.Point(5, 82);
            this.multiCommandLine.Multiline = true;
            this.multiCommandLine.Name = "multiCommandLine";
            this.multiCommandLine.Size = new System.Drawing.Size(295, 366);
            this.multiCommandLine.TabIndex = 25;
            this.multiCommandLine.KeyDown += new System.Windows.Forms.KeyEventHandler(this.multiCommandLine_KeyDown);
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.Gray;
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.Location = new System.Drawing.Point(5, 480);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(170, 86);
            this.button1.TabIndex = 27;
            this.button1.Text = "Run";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.run_Click);
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(5, 455);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(295, 22);
            this.textBox1.TabIndex = 26;
            this.textBox1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textBox1_KeyDown);
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.SystemColors.Window;
            this.pictureBox1.Location = new System.Drawing.Point(306, 43);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(850, 405);
            this.pictureBox1.TabIndex = 28;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Paint += new System.Windows.Forms.PaintEventHandler(this.pictureBox1_Paint);
            // 
            // timer1
            // 
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // fileNamePanel
            // 
            this.fileNamePanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.fileNamePanel.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.fileNamePanel.Controls.Add(this.fileCloseBtn);
            this.fileNamePanel.Controls.Add(this.fileNameLabel);
            this.fileNamePanel.Location = new System.Drawing.Point(5, 42);
            this.fileNamePanel.Name = "fileNamePanel";
            this.fileNamePanel.Size = new System.Drawing.Size(295, 34);
            this.fileNamePanel.TabIndex = 32;
            // 
            // fileCloseBtn
            // 
            this.fileCloseBtn.BackColor = System.Drawing.Color.Transparent;
            this.fileCloseBtn.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("fileCloseBtn.BackgroundImage")));
            this.fileCloseBtn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.fileCloseBtn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.fileCloseBtn.FlatAppearance.BorderSize = 0;
            this.fileCloseBtn.FlatAppearance.MouseDownBackColor = System.Drawing.Color.White;
            this.fileCloseBtn.FlatAppearance.MouseOverBackColor = System.Drawing.Color.DarkRed;
            this.fileCloseBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.fileCloseBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.fileCloseBtn.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.fileCloseBtn.Location = new System.Drawing.Point(260, -1);
            this.fileCloseBtn.Name = "fileCloseBtn";
            this.fileCloseBtn.Size = new System.Drawing.Size(28, 27);
            this.fileCloseBtn.TabIndex = 14;
            this.fileCloseBtn.UseVisualStyleBackColor = false;
            this.fileCloseBtn.Click += new System.EventHandler(this.FileCloseBotton_Click);
            // 
            // errorDisplay
            // 
            this.errorDisplay.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.errorDisplay.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold);
            this.errorDisplay.ForeColor = System.Drawing.Color.Black;
            this.errorDisplay.Location = new System.Drawing.Point(306, 486);
            this.errorDisplay.Name = "errorDisplay";
            this.errorDisplay.ReadOnly = true;
            this.errorDisplay.Size = new System.Drawing.Size(850, 80);
            this.errorDisplay.TabIndex = 33;
            this.errorDisplay.Text = "No Errors Found";
            // 
            // errorHeaderLabel
            // 
            this.errorHeaderLabel.AutoSize = true;
            this.errorHeaderLabel.BackColor = System.Drawing.SystemColors.Window;
            this.errorHeaderLabel.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.errorHeaderLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.errorHeaderLabel.ForeColor = System.Drawing.Color.Black;
            this.errorHeaderLabel.ImageAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.errorHeaderLabel.Location = new System.Drawing.Point(306, 451);
            this.errorHeaderLabel.Name = "errorHeaderLabel";
            this.errorHeaderLabel.Padding = new System.Windows.Forms.Padding(5, 3, 5, 3);
            this.errorHeaderLabel.Size = new System.Drawing.Size(79, 32);
            this.errorHeaderLabel.TabIndex = 34;
            this.errorHeaderLabel.Text = "0 Error";
            // 
            // syntaxBtn
            // 
            this.syntaxBtn.BackColor = System.Drawing.Color.Gray;
            this.syntaxBtn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.syntaxBtn.FlatAppearance.BorderSize = 0;
            this.syntaxBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.syntaxBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.syntaxBtn.Location = new System.Drawing.Point(188, 504);
            this.syntaxBtn.Name = "syntaxBtn";
            this.syntaxBtn.Size = new System.Drawing.Size(112, 58);
            this.syntaxBtn.TabIndex = 35;
            this.syntaxBtn.Text = "Syntax Check";
            this.syntaxBtn.UseVisualStyleBackColor = false;
            this.syntaxBtn.Click += new System.EventHandler(this.syntaxBtn_Click);
            // 
            // saveDrawMenuItem
            // 
            this.saveDrawMenuItem.Name = "saveDrawMenuItem";
            this.saveDrawMenuItem.Size = new System.Drawing.Size(224, 26);
            this.saveDrawMenuItem.Text = "Save Draw";
            this.saveDrawMenuItem.Click += new System.EventHandler(this.saveDrawMenuItem_Click);
            // 
            // loadDrawToolStripMenuItem
            // 
            this.loadDrawToolStripMenuItem.Name = "loadDrawToolStripMenuItem";
            this.loadDrawToolStripMenuItem.Size = new System.Drawing.Size(224, 26);
            this.loadDrawToolStripMenuItem.Text = "Load Draw";
            this.loadDrawToolStripMenuItem.Click += new System.EventHandler(this.loadDrawToolStripMenuItem_Click);
            // 
            // GPL
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.ClientSize = new System.Drawing.Size(1162, 574);
            this.Controls.Add(this.syntaxBtn);
            this.Controls.Add(this.errorHeaderLabel);
            this.Controls.Add(this.errorDisplay);
            this.Controls.Add(this.fileNamePanel);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.multiCommandLine);
            this.Controls.Add(this.menuStrip1);
            this.Name = "GPL";
            this.Text = "GPL";
            this.Load += new System.EventHandler(this.GPL_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.fileNamePanel.ResumeLayout(false);
            this.fileNamePanel.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label fileNameLabel;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileMenu;
        private System.Windows.Forms.ToolStripMenuItem loadMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveMenuItem;
        private System.Windows.Forms.TextBox multiCommandLine;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Panel fileNamePanel;
        private System.Windows.Forms.Button fileCloseBtn;
        private System.Windows.Forms.RichTextBox errorDisplay;
        private System.Windows.Forms.Label errorHeaderLabel;
        private System.Windows.Forms.Button syntaxBtn;
        private System.Windows.Forms.ToolStripMenuItem saveDrawMenuItem;
        private System.Windows.Forms.ToolStripMenuItem loadDrawToolStripMenuItem;
    }
}