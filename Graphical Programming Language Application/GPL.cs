﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;

namespace Graphical_Programming_Language_Application
{
    public partial class GPL : Form
    {
        //bitmap to be displayed on the output picture box
        Bitmap outputBitmap = new Bitmap(800, 400);

        //bitmap to display cursor point also to be displayed on the output picturebox
        Bitmap cursorBitmap = new Bitmap(800, 400);

        //Canvas reference to draw on the bitmap graphics
        Canvas outputCanvas;

        //Parser reference to parse the commands to call approriate shape methods
        Parser parser;

        //Checks the syntax of the commands
        SyntaxCheck syntaxChk;

        //Checks if multicommandline was requested to run
        public static bool multiCommandRun;

        //Stores all parse exceptions during execution of the command
        public StringBuilder parseExceptions;

        //number of errors encountered while parsing
        public int errorCount;

        //Stores the path of the file currently loaded
        String loadedfilePath;

        //Stores the name of the file currently loaded
        String loadedFileName;

        //For Thread
        Thread newThread;
        bool flag = false, running = false;

        //Default constructor for the GPLForm class
        public GPL()
        {
            InitializeComponent();
            
            outputCanvas = new Canvas(Graphics.FromImage(outputBitmap)); //class for handling the drawing, pass the drawing surface to it
            updateCursor(Graphics.FromImage(cursorBitmap)); //updates the cursor position of the canvas

            //initially no request is made to run multi command lines
            multiCommandRun = false;

            //by default no file's been loaded
            this.loadedfilePath = "";
            this.loadedFileName = "";

            //For Thread
            newThread = new System.Threading.Thread(thread);//create newthread passing the deligate method() which corresponds to the ThreadStart delegate(void method())
            newThread.Start();//make the thread execute
        }

        //for Thread
        public void thread()
        //this is the actual methid that is executed as a thread, if ou akllow execution to exit then the thread will terminate
        {
            while (true)//dont allow (in this case) for it to terminate{
            {
                while (running == true)
                {
                    if (flag == false)
                    {
                        this.button1.BackColor = System.Drawing.Color.Red;
                        flag = true;
                    }
                    else
                    {
                        this.button1.BackColor = System.Drawing.SystemColors.ActiveBorder;
                        flag = false;
                    }
                    Thread.Sleep(1000);
                }
            }
        }

        //updates the cursor pointer on the cursor bitmap's graphics to the current coordinates in output canvas object
        public void updateCursor(Graphics graphics)
        {
            int x = outputCanvas.XPos;
            int y = outputCanvas.YPos;
            graphics.Clear(Color.Transparent);//clears and makes a fresh transparent bitmap off cursor bitmap graphics
            SolidBrush sb = new SolidBrush(Color.Red);//solidbrush to draw cursor circle
            //graphics.DrawEllipse(outputCanvas.pen, outputCanvas.XPos-5, outputCanvas.YPos-5, 9, 9);   //hollow cursor

            if (!(x == 0 && y == 0))
            {
                //decreases the starting point by half to center the circular point with the lines drawn
                x -= 6;
                y -= 6;
            }

            graphics.FillEllipse(sb, x, y, 11, 11);//Draws Filled Ellipse at the current position set in canvas
        }

        //action listener when the form loads initially
        private void GPL_Load(object sender, EventArgs e)
        {
            timer1.Interval = 1000;
            timer1.Start();
        }

        //action listener when enter is pressed while inside the single command line
        private void textBox1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter) //if enter key is pressed
            {
                execute();
            }

        }

        //action listener when the run button is clicked
        private void run_Click(object sender, EventArgs e)
        {
            //for thread
            running = !running;
            //yikes!! 
            //newThread.Abort();

            //For program
            execute();
            executeSyntaxCheck();
        }

        //executes run method by differentiating whether it's for a single or multiline command
        public void execute()
        {
            String command = textBox1.Text.Trim().ToLower();//read commandline, get rid of trailing spaces and convert to lower case
            errorCount = 0; //reset error count for every new single line program
            parseExceptions = new StringBuilder();
            run(command, 1);//calls the run function to execute command
            if (multiCommandRun)
            {
                errorCount = 0;//reset error count for every new multi line program
                int lineNo = 1;//set the line no to 1
                parseExceptions = new StringBuilder();
                foreach (String line in multiCommandLine.Lines)
                {
                    String commandStatement = line.Trim().ToLower(); //read commandline, get rid of trailing spaces and convert to lower case
                    run(commandStatement, lineNo);
                    lineNo++;
                }
                multiCommandRun = false;
            }
            executeSyntaxCheck();
            updateErrorDisplay();
        }


        //executes syntax check method by differentiating whether it's for a single or multiline command
        public void executeSyntaxCheck()
        {
            if (multiCommandLine.Text != "")
                multiCommandRun = true;
            String command = textBox1.Text.Trim().ToLower();//read commandline, get rid of trailing spaces and convert to lower case
            errorCount = 0; //reset error count for every new single line program
            parseExceptions = new StringBuilder();
            syntaxCheck(command, 1);//calls the run function to execute command
            if (multiCommandRun)
            {
                errorCount = 0;  //reset error count for every new multi line program
                int lineNo = 1;//set the line no to 1
                parseExceptions = new StringBuilder();
                foreach (String line in multiCommandLine.Lines)
                {
                    String commandStatement = line.Trim().ToLower(); //read commandline, get rid of trailing spaces and convert to lower case
                    syntaxCheck(commandStatement, lineNo);
                    lineNo++;
                }
                multiCommandRun = false;
            }
            updateErrorDisplay(); //Updates the text shown in the error display text box
        }

        //run method that runs the commands from the command line by parsing and executing it
        public void run(String command, int lineNo)
        {
            ShapeFactory factory = new ShapeFactory();
            ArrayList shapes = new ArrayList();
            parser = new Parser();
            
            try
            {
                parser.parseCommand(command, outputCanvas, lineNo, factory, shapes);//parses and separates the command and parameters from command line
                Shape s = (Shape)shapes[0];
                s.draw(outputCanvas.graphics);
            }
            catch (InvalidCommandException e)                                        //when command name is invalid
            {
                errorCount++;
                parseExceptions.Append(e.Message + "\n");
            }
            catch (ArgumentException)
            {
                Console.WriteLine("");
            }
            catch (ParameterFormatException e)                                          //when the format of the parameter is wrong
            {
                errorCount++;
                parseExceptions.Append(e.Message + "\n");
            }
            catch (InvalidParameterLengthException e)                       //when the length of parameters is wrong for right command
            {
                errorCount++;
                parseExceptions.Append(e.Message + "\n");
            }
            catch (InvalidPenSizeException e)                                    //then the size of the pen is not within given range
            {
                errorCount++;
                parseExceptions.Append(e.Message + "\n");
            }
            catch (InvalidColorException e)                                    //when the color specified is not valid
            {
                errorCount++;
                parseExceptions.Append(e.Message + "\n");
            }

            updateCursor(Graphics.FromImage(cursorBitmap));
            //refreshPenBrushIndicators();
            Refresh();
        }

        //Checks the syntax of each commands in the program
        public void syntaxCheck(String command, int lineNo)//when command name is invalid
        {
            syntaxChk = new SyntaxCheck();
            try
            {
                syntaxChk.checkCommand(command, lineNo);//parses and separates the command and parameters from command line
            }
            catch (InvalidCommandException e)
            {
                errorCount++;
                parseExceptions.Append(e.Message + "\n");
            }
            catch (InvalidSyntaxException e)                                     //when the overall syntax is wrong
            {
                errorCount++;
                parseExceptions.Append(e.Message + "\n");
            }
            catch (ParameterFormatException e)                                      //when the format of the parameter is wrong
            {
                errorCount++;
                parseExceptions.Append(e.Message + "\n");
            }
            catch (InvalidParameterLengthException e)                       //when the length of parameters is wrong for right command
            {
                errorCount++;
                parseExceptions.Append(e.Message + "\n");
            }
            catch (InvalidPenSizeException e)                                    //then the size of the pen is not within given range
            {
                errorCount++;
                parseExceptions.Append(e.Message + "\n");
            }
            catch (InvalidColorException e)                                    //when the color specified is not valid
            {
                errorCount++;
                parseExceptions.Append(e.Message + "\n");
            }

            updateCursor(Graphics.FromImage(cursorBitmap));//updates the cursor position

            errorDisplay.Text = parseExceptions.ToString();
            Refresh();
        }

        //Updates the text shown in the error display text box
        public void updateErrorDisplay()
        {
            if (errorCount == 0)
            {
                errorDisplay.Text = "No Errors Found";//if parsed successfully, add no error found message to error display
                //sets font color of error display to green if no error found
                errorDisplay.ForeColor = System.Drawing.Color.Green;
                errorHeaderLabel.ForeColor = System.Drawing.Color.Green;
            }
            else
            {
                //sets font color of error display to red if error found
                errorDisplay.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(251)))), ((int)(((byte)(60)))), ((int)(((byte)(26)))));
                errorHeaderLabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(243)))), ((int)(((byte)(146)))), ((int)(((byte)(51)))));
            }
            errorHeaderLabel.Text = errorCount + " Errors";
        }


        //Setting timer for different color on background
        private void timer1_Tick(object sender, EventArgs e)
        {
            //add some random colors
            Random rn = new Random();
            int R, G, B;
            R = rn.Next(0, 255);
            G = rn.Next(0, 255);
            B = rn.Next(0, 255);

            BackColor = Color.FromArgb(B, R, G);//GPL form
            //pictureBox1.BackColor = Color.FromArgb(R, G, B);//Picture box
            //button1.BackColor = Color.FromArgb(R, G, B);//Run button
        }

        //Decides whether to add editing symbol i.e. * to the end of the file name label if a file is being loaded
        private void multiCommandLine_KeyDown(object sender, KeyEventArgs e)
        {
            if (!this.loadedfilePath.Equals(""))
            {
                // Stores the file's content to a variable
                string fileText = File.ReadAllText(this.loadedfilePath);

                //Compare the cotent of multiline command and file's content
                if (!fileText.Equals(multiCommandLine.Text))
                    this.fileNameLabel.Text = this.loadedFileName + "*";
                else
                    fileNameLabel.Text = this.loadedFileName;
            }

        }



        private void loadMenuItem_Click(object sender, EventArgs e)
        {
            var fileContent = string.Empty;
            var filePath = string.Empty;

            using (OpenFileDialog openFileDialog = new OpenFileDialog())
            {
                openFileDialog.InitialDirectory = "d:\\";
                openFileDialog.Filter = "txt files (*.txt)|*.txt|All files (*.*)|*.*";
                openFileDialog.FilterIndex = 2;
                openFileDialog.RestoreDirectory = true;

                if (openFileDialog.ShowDialog() == DialogResult.OK)
                {
                    //Get the path of specified file
                    filePath = openFileDialog.FileName;

                    //Read the contents of the file into a stream
                    var fileStream = openFileDialog.OpenFile();

                    using (StreamReader reader = new StreamReader(fileStream))
                    {
                        fileContent = reader.ReadToEnd();
                        multiCommandLine.Text = fileContent;
                        fileNameLabel.Text = Path.GetFileName(openFileDialog.FileName);//display the file name to the file name label

                        //store the file's name and path to the object's internal instance variables
                        this.loadedFileName = Path.GetFileName(openFileDialog.FileName); ;
                        this.loadedfilePath = openFileDialog.FileName;
                    }
                }
            }
        }

        //Writes the contents of multi command line to the file bieing saved
        private void saveFileMenuItem_Click(object sender, EventArgs e)
        {
            //Stream myStream;
            StreamWriter fWriter;//streamwriter to write to the file
            SaveFileDialog saveFileDialog1 = new SaveFileDialog();//open save file dialog

            saveFileDialog1.Title = "Save Gpl Text Files";//save dialog title extension
            saveFileDialog1.DefaultExt = ".gpl.txt";

            saveFileDialog1.Filter = "txt files (*.txt)|*.txt|All files (*.*)|*.*";
            saveFileDialog1.FilterIndex = 1;

            saveFileDialog1.RestoreDirectory = true;

            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                //write the contents of multi command line text box to file being saved
                fWriter = File.CreateText(saveFileDialog1.FileName);
                fWriter.Write(multiCommandLine.Text);
                fWriter.Close();

                //Sets the file name to the file name label
                if (!fileNameLabel.Text.Equals(Path.GetFileName(saveFileDialog1.FileName)))
                {
                    fileNameLabel.Text = Path.GetFileName(saveFileDialog1.FileName);           //display the file name to the file name label

                    //store the file's name and path to the object's internal instance variables
                    this.loadedFileName = Path.GetFileName(saveFileDialog1.FileName);
                    this.loadedfilePath = saveFileDialog1.FileName;
                }
            }

        }

        //Save draw shape of bitmap
        private void saveDrawMenuItem_Click(object sender, EventArgs e)
        {
            SaveFileDialog sf = new SaveFileDialog();
            sf.Filter = "JPG(*.JPG)|*.jpg";
            sf.RestoreDirectory = true;
            if (sf.ShowDialog() == DialogResult.OK)
            {
                var fileName = sf.FileName;
                outputBitmap.Save(fileName);
            }
        }
        //Load Picture on Picture Box.
        private void loadDrawToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var fileContent = string.Empty;
            using (OpenFileDialog openFileDialog = new OpenFileDialog())
            {
                openFileDialog.InitialDirectory = "d:\\";
                openFileDialog.Filter = "JPG(*.JPG)|*.jpg";
                openFileDialog.FilterIndex = 2;
                openFileDialog.RestoreDirectory = true;

                if (openFileDialog.ShowDialog() == DialogResult.OK)
                {                  
                    string filePath = openFileDialog.FileName;
                    pictureBox1.Image = Image.FromFile(filePath);

                    //Read the contents of the file into a stream
                    var fileStream = openFileDialog.OpenFile();
                    using (StreamReader reader = new StreamReader(fileStream))
                    {
                        fileContent = reader.ReadToEnd();
                        //multiCommandLine.Text = fileContent;
                        fileNameLabel.Text = Path.GetFileName(openFileDialog.FileName);//display the file name to the file name label

                        //store the file's name and path to the object's internal instance variables
                        this.loadedFileName = Path.GetFileName(openFileDialog.FileName); ;
                        this.loadedfilePath = openFileDialog.FileName;
                    }
                }
            }
        }


        //changes font color of file menu when mouse enters
        private void fileMenu_MouseEnter(object sender, EventArgs e)
        {
            fileMenu.ForeColor = Color.Black;
        }

        //changes font color of file menu when mouse leaves
        private void fileMenu_MouseLeave(object sender, EventArgs e)
        {
            fileMenu.ForeColor = Color.Black;
        }



        //When the close button is clicked to close the opening file and delete its contents from multi command line
        private void FileCloseBotton_Click(object sender, EventArgs e)
        {
            if (!fileNameLabel.Equals("Untitled.gpl.txt"))
            {
                fileNameLabel.Text = "Untitled.gpl.txt";
                multiCommandLine.Text = "";
                this.loadedfilePath = "";
                this.loadedFileName = "";
            }
        }

        //syntax check botton click
        private void syntaxBtn_Click(object sender, EventArgs e)
        {
            executeSyntaxCheck();
        }









        //Paints action to be performed on output screen picture box
        private void pictureBox1_Paint(object sender, PaintEventArgs e)
        {
            Graphics g = e.Graphics;//get graphics context of form (which is being displayed)

            g.DrawImageUnscaled(outputBitmap, 0, 0);//put the off screen bitmap on the picture box
            g.DrawImageUnscaled(cursorBitmap, 0, 0);//put the cursor bitmap on the picture box
        }
    }
}
