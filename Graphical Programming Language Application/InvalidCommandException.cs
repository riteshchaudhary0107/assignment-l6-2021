﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Graphical_Programming_Language_Application
{
    class InvalidCommandException : System.Exception
    {
        public InvalidCommandException() : base() { }
        public InvalidCommandException(int message) : base(String.Format("Invalid Command Exception at Line: {0}", message)) { }
        public InvalidCommandException(string message, System.Exception inner) : base(message, inner) { }

        // A constructor is needed for serialization when an
        // exception propagates from a remoting server to the client.
        protected InvalidCommandException(System.Runtime.Serialization.SerializationInfo info,
            System.Runtime.Serialization.StreamingContext context) : base(info, context) { }
    }
}
