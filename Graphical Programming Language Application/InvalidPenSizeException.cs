﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Graphical_Programming_Language_Application
{
    [Serializable]
    class InvalidPenSizeException : System.Exception
    {
        public InvalidPenSizeException() : base() { }
        public InvalidPenSizeException(int message) : base(String.Format("Invalid Pen Size Exception at Line: {0}", message)) { }
        public InvalidPenSizeException(string message, System.Exception inner) : base(message, inner) { }

        // A constructor is needed for serialization when an
        // exception propagates from a remoting server to the client.
        protected InvalidPenSizeException(System.Runtime.Serialization.SerializationInfo info,
            System.Runtime.Serialization.StreamingContext context) : base(info, context) { }
    }
}
