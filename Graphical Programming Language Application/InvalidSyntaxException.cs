﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Graphical_Programming_Language_Application
{
    [Serializable]
    class InvalidSyntaxException : System.Exception
    {
        public InvalidSyntaxException() : base() { }
        public InvalidSyntaxException(int message) : base(String.Format("Invalid Syntax Exception at Line: {0}", message)) { }
        public InvalidSyntaxException(string message, System.Exception inner) : base(message, inner) { }

        // A constructor is needed for serialization when an
        // exception propagates from a remoting server to the client.
        protected InvalidSyntaxException(System.Runtime.Serialization.SerializationInfo info,
            System.Runtime.Serialization.StreamingContext context) : base(info, context) { }
    }
}

