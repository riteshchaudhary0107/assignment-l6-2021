﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Graphical_Programming_Language_Application
{
    public partial class PanelClick : Form
    {
        public PanelClick()
        {
            InitializeComponent();
        }
        int icount = 0;
        ShapesPanel[] sp = new ShapesPanel[100];//An array of references to 'interface' Shapes
        Graphics g = null;

        private void panel1_Paint(object sender, PaintEventArgs e)
        {
            Graphics g = e.Graphics;//Get the graphics object
            Pen myPen = new System.Drawing.Pen(System.Drawing.Color.Black);
            //Use Pen
            int i = 0;
            for (i = 0; i < icount; i++)
            {
                sp[i].Draw(g, myPen); //call to polymorphic method Draw
            }
            myPen.Dispose();
        }

        private void panel1_MouseClick(object sender, MouseEventArgs e)
        {
            int x;
            int y;
            x = e.X;//MouseEventArgs object e contains mouse coordinates
            y = e.Y;

            if (radioButtonCircle.Checked)
            {
                sp[icount] = new CirclePanel();//Add a circle, or a rectangle, Depending on status of radio buttons

            }
            else
            {
                sp[icount] = new RectanglePanel();
            }
            sp[icount].SetPosition(x, y);//Set position of shape using polymorphic method 'SetPosition'
            icount++;
            label2.Text = icount.ToString();
            panel1.Refresh();//Force a repaint

        }

        private void PanelClick_Load(object sender, EventArgs e)
        {

        }

        //private void panel1_Paint_1(object sender, PaintEventArgs e)
        //{

        //}
    }
}
