﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;
using System.Windows.Forms;

namespace Graphical_Programming_Language_Application
{
    public class Parser
    {
        //default constructor
        public Parser()
        {

        }

        //parses the string line written on the command lines and deduces and executes the underlying commands
        public void parseCommand(String line, Canvas outputCanvas, int lineNo, ShapeFactory factory, ArrayList shapes)
        {

            //if the line is empty
            if (line.Equals(""))
                return;

            line = line.ToLower().Trim();//trimm the trailing whitespaces from the command
            String[] splitLine = line.Split(' ');//split the command into the command name and its parameters
            String command = splitLine[0];//first part stored as command
            if (splitLine.Length > 2)
            {
                throw new InvalidSyntaxException(lineNo);//when the overall syntax is wrong
            }
            //If invalid command is typed
            if (!(command.Equals("drawto") || command.Equals("square") || command.Equals("rect") || command.Equals("circle") || command.Equals("moveto") ||
                command.Equals("triangle") || command.Equals("halftriangle") || command.Equals("run") || command.Equals("clear") || command.Equals("reset") ||
                command.Equals("pensize") || command.Equals("pencolor") || command.Equals("fill") || command.Equals("fillcolor")
                || command.Equals("shaperectangle")))
            {
                throw new InvalidCommandException(lineNo);  //when command name is invalid
            }

            //if there exists the parameters along with the command
            else if (splitLine.Length > 1)
            {
                String[] parameters = splitLine[1].Split(','); //second part stored in array to be further split by comma
                int[] parametersInt = new int[parameters.Length];//int array for storing parameters

                //if parameters are not supposed to take string parameters
                if (!(command.Equals("pencolor") || command.Equals("fill") || command.Equals("fillcolor")))
                {
                    for (int i = 0; i < parameters.Length; i++)
                    {
                        try
                        {
                            parametersInt[i] = Convert.ToInt32(parameters[i].Trim()); //convert string parameters to int 
                        }
                        catch (FormatException)
                        {
                            throw new ParameterFormatException(lineNo); //when the format of the parameter is wrong
                        }
                    }
                }



                //Switch cases to call associated methods for different commands with parameters
                switch (command)
                {
                    case "shaperectangle":
                        shapes.Add(factory.GetShape("shaperectangle"));
                        break;
                    case "pensize": //sets the width of the pen
                        checkParameterLength(1, parametersInt, lineNo); //check parameter length before running the comamnd
                        outputCanvas.setPenSize(parametersInt[0], lineNo);
                        break;
                    case "pencolor": //set the color of the pen
                        checkStringParameterLength(1, parameters, lineNo);//check parameter length before running the comamnd
                        outputCanvas.setPenColor(parameters[0], lineNo);
                        break;
                    case "fill"://set the color of the pen
                        checkStringParameterLength(1, parameters, lineNo); //check parameter length before running the comamnd
                        outputCanvas.setFillStatus(parameters[0], lineNo);
                        break;
                    case "square"://draws Square on output canvas
                        checkParameterLength(1, parametersInt, lineNo);//check parameter length before running the comamnd
                        outputCanvas.drawSquare(parametersInt[0]);
                        break;
                    case "triangle"://draws Triangle on output canvas
                        checkParameterLength(2, parametersInt, lineNo);//check parameter length before running the comamnd
                        outputCanvas.drawTriangle(parametersInt[0], parametersInt[1]);
                        break;
                    case "halftriangle"://draws Triangle on output canvas
                        checkParameterLength(2, parametersInt, lineNo);//check parameter length before running the comamnd
                        outputCanvas.drawHalfTriangle(parametersInt[0], parametersInt[1]);
                        break;
                    case "rect"://draws Rectangle on output canvas
                        checkParameterLength(2, parametersInt, lineNo); //check parameter length before running the comamnd
                        outputCanvas.drawRectangle(parametersInt[0], parametersInt[1]);
                        break;
                    case "circle":  //draws Rectangle on output canvas
                        checkParameterLength(1, parametersInt, lineNo); //check parameter length before running the comamnd
                        outputCanvas.drawCircle(parametersInt[0]);
                        break;
                    case "drawto"://draws Lilne on output canvas
                        checkParameterLength(2, parametersInt, lineNo);//check parameter length before running the comamnd
                        outputCanvas.drawTo(parametersInt[0], parametersInt[1]);
                        break;
                    case "moveto": //move cursor position on output canvas
                        checkParameterLength(2, parametersInt, lineNo);//check parameter length before running the comamnd
                        outputCanvas.moveTo(parametersInt[0], parametersInt[1]);
                        break;
                    case "clear": //if clear command is typed but with parameters                                
                        throw new InvalidParameterLengthException(lineNo);  //throw new wrong parameter number exception
                    case "run"://if clear command is typed but with parameters                                   
                        throw new InvalidParameterLengthException(lineNo);//throw new wrong parameter number exception
                    case "reset"://if reset command is typed but with parameters                                   
                        throw new InvalidParameterLengthException(lineNo);//throw new wrong parameter number exception
                    default://default
                        throw new InvalidCommandException(lineNo);  //when command name is invalid                        
                }
            }
            //for single word commands
            else
            {
                //Switch cases to call associated methods for different commands without parameters
                switch (command)
                {
                    case "clear": //clears the screen of output canvas                                  
                        outputCanvas.clear();
                        break;
                    case "run"://runs the program written on multi line command box                                  
                        GPL.multiCommandRun = true;
                        break;
                    case "reset"://resets the pen and brush                                
                        outputCanvas.reset();
                        break;
                    default://set errorfound to true
                        throw new InvalidCommandException(lineNo);  //when command name is invalid
                }
            }

        }

        //Method for Unit Test only
        public void checkParameterLengths(int v1, int v2, int v3)
        {
            throw new NotImplementedException();
        }

        //check if the number of parameters provided is right for this command
        public static void checkParameterLength(int pLength, int[] parametersInt, int lineNo)
        {
            if (parametersInt.Length != pLength)
                throw new InvalidParameterLengthException(lineNo);//throw new wrong parameter number exception
        }

        //check if the number of string parameters provided is right for this command
        public static void checkStringParameterLength(int pLength, String[] parameters, int lineNo)
        {
            if (parameters.Length != pLength)
                throw new InvalidParameterLengthException(lineNo);//throw new wrong parameter number exception
        }
    }
}
