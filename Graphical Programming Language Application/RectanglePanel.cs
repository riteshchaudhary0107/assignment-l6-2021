﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace Graphical_Programming_Language_Application
{
    class RectanglePanel : ShapesPanel
    {
        private int x, y;
        //private const int size = 30;
        private const int width = 60;
        private const int height = 30;


        public void SetPosition(int x, int y)
        {
            this.x = x;
            this.y = y;
        }

        //Need a Pen onject and a Graphics object
        public void Draw(Graphics g, Pen myPen)
        {
            //no need to 'override' method in an interface
            g.DrawRectangle(myPen, x, y, width, height);
            SolidBrush b = new SolidBrush(Color.Red);
            g.FillRectangle(b, x, y, width, height);
            //Other parameters are x and y position,
            //and the horizontal and verticle size of the Rectangle

        }

        public override string ToString()
        {
            return "Rectangle class, x, y, size = " + x + " " + y;
        }
    }
}
