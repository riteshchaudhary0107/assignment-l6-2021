﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace Graphical_Programming_Language_Application
{
    public abstract class Shape : Shapes
    {
        protected Color color; //Shapes's colour
        protected int x, y; //Position Properties

        //default constructor definig initial cursor position and color
        public Shape()
        {
            //color = Color.Red;
            //x = y = 0;//set initial position to (0,0)
        }

        //parameterized constructor
        public Shape(Color colour, int x, int y)
        {

            this.color = colour; //shape's colour
            this.x = x;          //its x pos
            this.y = y;            //its y pos
            //can't provide anything else as "shape" is too general
        }

        //the three methods below are from the Shapes interface
        //here we are passing on the obligation to implement them to the derived classes by declaring them as abstract

        //Draws the shape on the graphics
        public abstract void draw(Graphics g);

        //Calculates the are of the shape
        public abstract double calcArea();

        //Calculates the parameter of the shape
        public abstract double calcPerimeter();

        //set is declared as virtual so it can be overridden by a more specific child version
        //but is here so it can be called by that child version to do the generic stuff
        //note the use of the param keyword to provide a variable parameter list to cope with some shapes having more setup information than others (in Java it is called varargs and uses the … notation
        public virtual void set(Color colour, params int[] list)
        {
            this.color = colour;
            this.x = list[0];
            this.y = list[1];
        }


        public override string ToString()
        {
            return base.ToString() + "    " + this.x + "," + this.y + " : ";
        }


    }
}
