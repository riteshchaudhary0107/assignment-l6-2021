﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Graphical_Programming_Language_Application
{
    public class ShapeFactory
    {//this shape factory contains all the shape objects
        public Shape GetShape(String shapeType)
        {
            shapeType = shapeType.ToUpper().Trim();//you could argue that
            //you want a specific word string to create an object but I'm allowing any case combination
        
            if (shapeType.Equals("SHAPERECTANGLE"))
            {
                return new Rectangle();//obj for shaperectangle
            }
            else if (shapeType.Equals("RECTANGLE"))
            {
                return new Rectangle();//obj for rectangle
            }
            else if (shapeType.Equals("CIRCLE"))
            {
                return new Circle();//obj for circle
            }
            else if (shapeType.Equals("SQUARE"))
            {
                return new Square();//obj for square
            }
            else if (shapeType.Equals("TRIANGLE"))
            {
                return new Triangle();//obj for triangle
            }
            else if (shapeType.Equals("HALFTRIANGLE"))
            {
                return new halfTriangle();//obj for halftriangle
            }
            else
            {
                //if we get here then what has been passed inis known so throw an appropriate exception
                System.ArgumentException argEx = new System.ArgumentException("Factory error: " + shapeType + " does not exit");
                throw argEx;
            }
        }
    }
}
