﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace Graphical_Programming_Language_Application
{
    interface ShapesPanel
    {
        void SetPosition(int x, int y);
        void Draw(Graphics g, Pen myPen);
        string ToString();
    }
}
