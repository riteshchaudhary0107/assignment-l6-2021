﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Graphical_Programming_Language_Application
{
    public partial class StudentRegis : Form
    {
        public StudentRegis()
        {
            InitializeComponent();
        }

        private void btn_Submit_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Data Submitted Successful*");

            string name = textBoxName.Text;
            string email = textBoxEmail.Text;
            string address = textBoxAddress.Text;
            string studentID = textBoxStudentID.Text;
            string course = comboBoxCourse.Text;
            string level = comboBoxLevel.Text;

            MessageBox.Show("Name : " + name
                + "\nEmail : " + email
                + "\nAddress : " + address
                + "\nStudent ID : " + studentID
                + "\nCourse : " + course
                + "\nLevel : " + level);
        }
    }
}
