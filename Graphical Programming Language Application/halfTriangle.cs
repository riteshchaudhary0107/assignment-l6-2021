﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace Graphical_Programming_Language_Application
{
    class halfTriangle : Shape
    {

        int width, height;
        public halfTriangle() : base()
        {
            width = 100;
            height = 100;
        }
        public halfTriangle(Color color, int x, int y, int width, int height) : base(color, x, y)
        {

            this.width = width; //the only thing that is different from shape
            this.height = height;
        }

        public override void set(Color color, params int[] list)
        {
            //list[0] is x, list[1] is y, list[2] is width, list[3] is height
            base.set(color, list[0], list[1]);
            this.width = list[2];
            this.height = list[3];

        }

        public override void draw(Graphics g)
        {
            Pen p = new Pen(Color.Black, 2);
            SolidBrush b = new SolidBrush(color);
            g.FillRectangle(b, x, y, width, height);
            g.DrawRectangle(p, x, y, width, height);
        }

        public override double calcArea()
        {
            return 0.5 * height * width;
        }

        public override double calcPerimeter()
        {
            return width + (2 * height);
        }



    }
}
