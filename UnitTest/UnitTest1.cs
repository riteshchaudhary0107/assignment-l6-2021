﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace UnitTest
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestMethod1()
        {
                Canvas c = new Canvas();
                parseline p = new parseline();
                TextBox rtb = new TextBox();
                rtb.Text = "if 100>10 \r\n" +
                    "drawto 150,100 \r\n" +
                    "endif";

                StringBuilder ErrorList = new StringBuilder();
                p.parseCommand(rtb, ErrorList, c, new PictureBox());

                int Expectedx = 150;
                int actualx = c.xPos;
                Assert.AreEqual(Expectedx, actualx, "test fail");

                int Expectedy = 100;
                int actualy = c.yPos;
                Assert.AreEqual(Expectedy, actualy, "test fail");
            
        }
    }
}
