﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Windows.Forms;
using Graphical_Programming_Language_Application;
using System.Text;
using System.Drawing;
using System.Collections;


namespace UnitTestProject1
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
       public void TestParsermoveto()
        {                   
            Canvas c = new Canvas();
            ShapeFactory sf = new ShapeFactory();
            Parser p = new Parser();
            ArrayList shapes = new ArrayList();
            int lineNo = 1;
            String line = "moveto 150,100 ";

            p.parseCommand(line, c, lineNo, sf, shapes);

            int Expectedx = 150;
            int actualx = c.XPos;
            Assert.AreEqual(Expectedx, actualx, "test fail");

            int Expectedy = 100;
            int actualy = c.YPos;
            Assert.AreEqual(Expectedy, actualy, "test fail");
        }



        [TestMethod]
        public void TestParserdrawto()
        {           
            Canvas c = new Canvas();
            ShapeFactory sf = new ShapeFactory();
            Parser p = new Parser();
            ArrayList shapes = new ArrayList();
            int lineNo = 1;
            String line = "drawto 150,100 ";

            p.parseCommand(line, c, lineNo, sf, shapes);

            int Expectedx = 150;
            int actualx = c.XPos;
            Assert.AreEqual(Expectedx, actualx, "test fail");

            int Expectedy = 100;
            int actualy = c.YPos;
            Assert.AreEqual(Expectedy, actualy, "test fail");
        }

        [TestMethod]
        public void checkParameterLength()
        {
            Parser p = new Parser();
            try
            {
                p.checkParameterLengths(1,2,3);
            }
            catch (Exception e)
            {
                Console.WriteLine("Error");
                return;
            }
            Assert.Fail("Fail to process the statement");
        }

        [TestMethod]
        public void testrun()
        {

        }

    }
}
